# Avant de marquer ma MR comme `Ready`



- [ ] Vérifier que les tests sont à jour

- [ ] Vérifier que la doc est à jour



# Avant de valider la MR



- [ ] Vérifier que les normes sont respectées

- [ ] Vérifier les rapports issus du pipeline