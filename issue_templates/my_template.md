## Résumé de l'issue



(décrire ici le contexte de l'issue)



## Type d'issue



- [ ] Bug

- [ ] Nouvelle fonctionnalité

- [ ] Amélioration



## Quelle(s) version(s) impactée(s) ?



(préciser si une ou plusieurs versions sont impactées par l'issue)